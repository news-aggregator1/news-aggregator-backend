"""Implements REST."""
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from typing import List

import database.crud as crud
import database.db as db
import models.news as news

router = APIRouter()


@router.get("/news", response_model=List[news.News])
async def get_news(pageNumber: int = 1, db: Session = Depends(db.get_db)):
    """Get page of news."""
    return crud.get_articles(db, offset=20 * (pageNumber - 1))


@router.get("/news/number")
async def get_number_of_news(db: Session = Depends(db.get_db)):
    """Get number of news."""
    return crud.get_article_count(db)


@router.get("/news/{tag_id}", response_model=List[news.News])
async def get_tagged_news(tag_id: int, db: Session = Depends(db.get_db)):
    """Get page of news filtered by tag."""
    return crud.get_articles_by_category(db, tag_id)
