"""Implements REST."""
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

import lipsum
import database.crud as crud
import database.db as db
from models.comment import Comment, CommentCreate

router = APIRouter()


@router.post("/comment", response_model=Comment)
async def post_comment(comment: CommentCreate, db: Session = Depends(db.get_db)):
    """Post new comment."""
    return crud.add_comment(db=db, comment=comment)


@router.delete("/comment")
async def delete_comment(commentId: int, db: Session = Depends(db.get_db)):
    """Delete comment."""
    crud.delete_comment(db=db, comment_id=commentId)


@router.get("/comment/populate")
async def populate_comments(db: Session = Depends(db.get_db)):
    """Post random comments."""
    for post in crud.get_articles(db):
        comment = CommentCreate(
            **{
                "authorEmail": "test@test.com",
                "newsId": post.id,
                "text": lipsum.generate_words(20),
            }
        )
        crud.add_comment(db=db, comment=comment)
