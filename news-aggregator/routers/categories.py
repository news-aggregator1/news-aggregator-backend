"""Implements REST."""
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from typing import List

import database.crud as crud
import database.db as db
import models.category as category

router = APIRouter()


@router.get("/categories", response_model=List[category.Category])
async def get_categories(db: Session = Depends(db.get_db)):
    """Get page of news."""
    return crud.get_categories(db)
