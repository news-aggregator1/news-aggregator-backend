"""Database ORM models."""
# from sqlalchemy.schema import Table
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship

from database.db import Base


# article_tags = Table(
#     "articles_by_tags",
#     Base.metadata,
#     Column("article_id", ForeignKey("article.id")),
#     Column("tag_id", ForeignKey("tag.id")),
# )


class Article(Base):
    """Article ORM model."""

    __tablename__ = "article"

    id = Column(Integer, primary_key=True, index=True)
    content = Column(String)
    description = Column(String)
    image_url = Column(String)
    publication_date = Column(DateTime)
    title = Column(String)
    url = Column(String)
    category_id = Column(Integer, ForeignKey("category.id"))
    source_id = Column(Integer, ForeignKey("source.id"))
    comments = relationship("Comment")
    tags = relationship("Category")
    # tags = relationship("Tag", secondary=article_tags)


# class Tag(Base):
#     """Tag ORM model."""

#     __tablename__ = "tag"

#     id = Column(Integer, primary_key=True, index=True)
#     name = Column(String)


class Category(Base):
    """Category ORM model."""

    __tablename__ = "category"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)


class Source(Base):
    """Source ORM model."""

    __tablename__ = "source"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)


class Comment(Base):
    """Comment ORM model."""

    __tablename__ = "comment"
    id = Column(Integer, primary_key=True, index=True)
    text = Column(String)
    author_email = Column(String)
    article_id = Column(Integer, ForeignKey("article.id"))
