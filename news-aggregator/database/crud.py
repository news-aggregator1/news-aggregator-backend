"""Database CRUD operations."""
from sqlalchemy.orm import Session
from sqlalchemy import func, desc

import database.models as models
from models.comment import CommentCreate


def get_articles(db: Session, offset: int = 0, limit: int = 20):
    """Select limit articles with offset."""
    return (
        db.query(models.Article)
        .order_by(desc(models.Article.publication_date))
        .filter(models.Article.image_url != "")
        .filter(models.Article.content != "")
        .distinct(models.Article.publication_date)
        .offset(offset)
        .limit(limit)
        .all()
    )


def get_article_count(db: Session):
    """Get amount of articles in db."""
    return (
        db.query(models.Article)
        .order_by(desc(models.Article.publication_date))
        .filter(models.Article.image_url != "")
        .filter(models.Article.content != "")
        .distinct(models.Article.publication_date)
        .count()
    )


def get_articles_by_category(
    db: Session, category_id: int, offset: int = 0, limit: int = 20
):
    """Select limit articles with offset filtered by category."""
    return (
        db.query(models.Article)
        .order_by(desc(models.Article.publication_date))
        .filter(models.Article.image_url != "")
        .filter(models.Article.content != "")
        .filter(models.Article.category_id == category_id)
        .distinct(models.Article.publication_date)
        .offset(offset)
        .limit(limit)
        .all()
    )


def get_categories(db: Session, offset: int = 0, limit: int = 20):
    """Select limit categories with offset."""
    count_ = func.count("*")
    return (
        db.query(models.Category)
        .join(models.Article)
        .filter(models.Article.image_url != "")
        .filter(models.Article.content != "")
        .order_by(count_.desc())
        .group_by(models.Category.id)
        .offset(offset)
        .limit(limit)
        .all()
    )


def add_comment(db: Session, comment: CommentCreate):
    """Add comment."""
    if db.query(models.Comment).count() > 0:
        id = db.query(func.max(models.Comment.id)).first()[0] + 1
    else:
        id = 0
    db_item = models.Comment(**comment.dict(), id=id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


def delete_comment(db: Session, comment_id: int):
    """Delete comment."""
    db.query(models.Comment).filter_by(id=comment_id).delete()
    db.commit()
