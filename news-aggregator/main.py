"""Backend start module."""
import uvicorn
from fastapi import FastAPI

from routers import comments, news, categories

app = FastAPI()

app.include_router(comments.router)
app.include_router(news.router)
app.include_router(categories.router)


@app.get("/")
async def root():
    """Placeholder."""
    return {"message": "News aggregator backend"}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
