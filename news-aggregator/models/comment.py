"""Contains JSON serialization models."""
from pydantic import BaseModel, EmailStr, Field


class CommentBase(BaseModel):
    """Comment base model class."""

    author_email: EmailStr = Field(alias="authorEmail")
    article_id: int = Field(alias="newsId")
    text: str


class CommentCreate(CommentBase):
    """Comment create class."""

    pass


class Comment(CommentBase):
    """Comment model class."""

    id: int

    class Config:
        """News model config."""

        allow_population_by_field_name = True
        orm_mode = True
