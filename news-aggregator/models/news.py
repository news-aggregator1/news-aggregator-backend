"""Contains JSON serialization models."""
from datetime import datetime
from pydantic import BaseModel, AnyHttpUrl, Field
from typing import Optional

# from models.tag import Tag
from models.comment import Comment
from models.category import Category


class News(BaseModel):
    """News model class."""

    id: int
    title: str
    publication_date: datetime = Field(alias="date")
    url: AnyHttpUrl = Field(alias="url")
    tags: Optional[Category]
    content: str = Field(alias="textPart")
    comments: Optional[list[Comment]]
    image_url: str = Field(alias="photo")

    class Config:
        """News model config."""

        allow_population_by_field_name = True
        orm_mode = True
