"""Contains JSON serialization models."""
from pydantic import BaseModel


class Category(BaseModel):
    """Category model class."""

    id: int
    name: str

    class Config:
        """News model config."""

        allow_population_by_field_name = True
        orm_mode = True
