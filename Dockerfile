FROM python:3.10
WORKDIR /app
RUN apt-get update && \
    apt-get install -y libpq-dev gcc
COPY ./requirements.txt .
COPY ./news-aggregator ./news-aggregator

RUN python -m pip install -r requirements.txt
CMD python news-aggregator/main.py
